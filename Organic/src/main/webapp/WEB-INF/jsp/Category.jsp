<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Organic</title>
<script type="text/javascript" src="js/jquery-2.1.4.js"></script> 

<script>
addCategory=function(){
	$("#categoryForm").submit();
}
</script>
</head>
<body >
<div id="bodydiv">
	
	<div style="font-family: sans-serif;font-size: 15px;"> 
	<form:form name="categoryForm" id="categoryForm" commandName="categoryCommand" method="post" action="savecategory.htm">
		<table width="90%" border="0" align="center">
			<tbody>
			<tr ><td >Category Name </td><td> <form:input   path="name" id="name"/></td>
			<tr ><td >Category Description </td><td> <form:textarea   path="description" id="description"/></td>
			<tr ><td >Parent Category </td><td> <form:select   path="parent" id="parent"/></td>
			<tr> <td> <input type="button" onclick="javaScript:addCategory()" name="Add Category" value="category"> </td>			
			</tr>
        
			
			
		</tbody></table>
	</form:form>
	</div>

		
	
		

</div>	
</body>

</html>
