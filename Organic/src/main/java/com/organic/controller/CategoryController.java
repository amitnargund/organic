package com.organic.controller;



import javax.servlet.http.HttpSession;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.organic.command.CategoryCommand;



@Controller
public class CategoryController {

	@RequestMapping(value = "savecategory.htm",method=RequestMethod.POST)
	public String savecategory(@ModelAttribute("categoryCommand") CategoryCommand categoryCommand,ModelMap modelMap,
			HttpSession session)throws Exception{
		
		String methodName="savecategory";
		String target = "casRegistration";
		
		
		modelMap.addAttribute("categoryCommand", categoryCommand);
		return target;
	}
}
