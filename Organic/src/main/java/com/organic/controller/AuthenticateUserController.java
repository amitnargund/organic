package com.organic.controller;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.organic.command.CategoryCommand;
import com.organic.dto.OrganicUser;
import com.organic.service.CategoryService;



@Controller
public class AuthenticateUserController {

	private Logger logger = Logger.getLogger(AuthenticateUserController.class);
	@Autowired
	private ServletContext servletContext;
	@Autowired
	private CategoryService categoryService;
	@RequestMapping(value="authenticateUser.htm", method = RequestMethod.GET)
	private ModelAndView authenticateUser(HttpSession session,ModelMap modelMap){

		logger.info(" :authenticateUser() : ENTRY");
		/*String redirectTo = "";
		System.out.println("-------authent");
		Authentication a = SecurityContextHolder.getContext().getAuthentication();
		SpectraUser currentUser = (SpectraUser)a.getPrincipal();*/
		logger.info(" :authenticateUser() : EXIT");
	
		CategoryCommand categoryCommand=new CategoryCommand();
		modelMap.addAttribute("categoryCommand", categoryCommand);
		return new ModelAndView("Category");
	
			
	}

}
