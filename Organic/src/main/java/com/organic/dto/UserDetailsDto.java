package com.organic.dto;

import java.io.Serializable;
import java.util.Map;

public class UserDetailsDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String userFName;
	
	private String userLName;
	
	private int userId;
	
	private String loginId;
	
	private String pswd;
	
	private Map<Integer, String> roles;
	
	private int roleId;
	
	private String roleName;

	public String getUserFName() {
		return userFName;
	}

	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}

	public String getUserLName() {
		return userLName;
	}

	public void setUserLName(String userLName) {
		this.userLName = userLName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	public Map<Integer, String> getRoles() {
		return roles;
	}

	public void setRoles(Map<Integer, String> roles) {
		this.roles = roles;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UserDetailsDto [userFName=" + userFName + ", userLName="
				+ userLName + ", userId=" + userId + ", loginId=" + loginId
				+ ", pswd=" + pswd + ", roles=" + roles + ", roleId=" + roleId
				+ ", roleName=" + roleName + "]";
	}
}
