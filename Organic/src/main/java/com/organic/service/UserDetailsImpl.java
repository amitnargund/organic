package com.organic.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.organic.dao.UserDao;
import com.organic.dto.RoleDetailsDto;
import com.organic.dto.OrganicUser;
import com.organic.dto.UserDetailsDto;

@Transactional
@Service("userDetailsService")
public class UserDetailsImpl implements UserDetailsService {

	private Logger logger = Logger.getLogger(UserDetailsImpl.class);
	
	@Autowired
	UserDao userDao;
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		logger.info(" >>: loadUserByUsername() : ENTRY");
		UserDetailsDto userVo = (UserDetailsDto) userDao.getUserByUsername(username);
		if (userVo == null)
	    	throw new UsernameNotFoundException("User not found");
		

	    logger.info(" : loadUserByUsername() : EXIT");
	    return buildUserFromUserEntity(userVo);
	}

	protected User buildUserFromUserEntity(UserDetailsDto userVo) {
		logger.info(" : buildUserFromUserEntity() : ENTRY");

	    String username = userVo.getLoginId();
	    String password = userVo.getPswd();
	    OrganicUser user = null;
	    boolean enabled = true;
	    boolean accountNonExpired = enabled;
	    boolean credentialsNonExpired = enabled;
	    boolean accountNonLocked = enabled;
	    try{
	    Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	   
	    for(Map.Entry<Integer, String> roleName:userVo.getRoles().entrySet()){
	      authorities.add(new GrantedAuthorityImpl("ROLE_"+roleName.getValue().toUpperCase()));
	    }
	    accountNonExpired=true;
	    
	    user = new OrganicUser(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	    user.setUserId(userVo.getUserId());
	    user.setLoginId(userVo.getLoginId());
	    user.setPswd(userVo.getPswd());
	    user.setRoles(userVo.getRoles());
	    user.setRoleId(userVo.getRoleId());
	    logger.info(" : buildUserFromUserEntity() : EXIT");
	    }catch(Exception e) {
	    	logger.error("Exception",e);
	    }
	    return user;
	  }

	public UserDetailsDto getUserInfo(String userName){
		return userDao.getUserByUsername(userName);
	}

	
}