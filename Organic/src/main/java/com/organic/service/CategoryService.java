package com.organic.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.organic.dao.CategoryDao;


public class CategoryService {
	@Autowired
	private CategoryDao categoryDao;
	
	public Map<Integer,String> getCategoryList()throws Exception{
		Map<Integer, String> otherCvMap=new HashMap<Integer, String>();
		
		List<Map<String, Object> > otherCvList=categoryDao.getCategoryList();
		
		for(Map<String, Object> map:otherCvList){
			otherCvMap.put((Integer)map.get("CATEGORY_ID"),(String)map.get("CATEGORY_NAME"));
		}
		return otherCvMap;
	}
}
