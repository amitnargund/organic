package com.organic.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class CategoryDao {
private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public List<Map<String, Object>> getCategoryList()throws Exception{
		String query="SELECT CATEGORY_ID,CATEGORY_NAME FROM CATEGORY WHERE PARENT=0";
		
		List<Map<String, Object>> list=jdbcTemplate.queryForList(query);
		return list;
	}
}
