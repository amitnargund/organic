package com.organic.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.organic.dto.UserDetailsDto;
import com.organic.rowmapper.UserDetailsRowMapper;

@Repository
public class UserDao {
	
	private static Logger logger = Logger.getLogger(UserDao.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public UserDetailsDto getUserByUsername(String userName){
		String methodName = "getUserByUsername()";

		logger.info("^^^^^^^^^^^^"+userName);
		String[] arr = userName.split("<br>");
		Map<Integer, String> roleMap=new HashMap<Integer, String>();
		roleMap.put(0, "USER");

		String query = "SELECT * FROM USERS WHERE USERS.LOGIN_ID = ? ";

		int result = -5;
		UserDetailsDto userDetailsVo=new UserDetailsDto();
		try {
			 userDetailsVo=jdbcTemplate.queryForObject(query,new UserDetailsRowMapper(), new Object[]{userName});
			 
		}
		catch (EmptyResultDataAccessException e) {
			result = 0;
		}
		catch (BadSqlGrammarException e) {
			logger.error( methodName+" : BadSqlGrammarException  "+e );
		}
		catch (DataAccessException e) {
			logger.error( methodName+" : DataAccessException  "+e );
		}

		if(result!=0){
			query = "SELECT ROLES.ROLE_ID, ROLE_NAME FROM ROLES, USER_ROLES " +
							"WHERE ROLES.ROLE_ID=USER_ROLES.ROLE_ID AND USER_ROLES.USER_ID=?";
			try {

				List<Map<String,Object>> map=jdbcTemplate.queryForList(query,userDetailsVo.getUserId());
				for(Map<String,Object> row:map){
					roleMap.put(Integer.valueOf(row.get("ROLE_ID").toString()), row.get("ROLE_NAME").toString());
				}
				}
			catch (EmptyResultDataAccessException e) {
				result = 0;
			}
			catch (BadSqlGrammarException e) {
				logger.error( methodName+" : BadSqlGrammarException  "+e );
			}
			catch (DataAccessException e) {
				logger.error( methodName+" : DataAccessException  "+e );
			}
		}
		
		userDetailsVo.setRoles(roleMap);
		userDetailsVo.setRoleId(Integer.parseInt(arr[1]));

		
		System.out.println("userDetailsVo");
		
		return userDetailsVo;
	}
	
	
	
}
