package com.organic.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.organic.dto.UserDetailsDto;

public class UserDetailsRowMapper implements RowMapper<UserDetailsDto>{
	
	public static final String PKG_USER_DETAILS = "PKG_USER_DETAILS";
	public static final String USER_ID_NAME = "USER_ID_NAME";

	private String type;
	public UserDetailsRowMapper(){}
	public UserDetailsRowMapper(String type){
		this.type = type;
	}
	public UserDetailsDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		//if(type == null){
			UserDetailsDto userDetailsVo = new UserDetailsDto();
			userDetailsVo.setUserId(rs.getInt("USER_ID"));
			userDetailsVo.setUserFName(rs.getString("USER_FIRST_NAME"));
			userDetailsVo.setUserLName(rs.getString("USER_LAST_NAME"));
			userDetailsVo.setLoginId(rs.getString("LOGIN_ID"));
			userDetailsVo.setPswd(rs.getString("PASSWD"));

			return userDetailsVo;
		/*}
		else if(PKG_USER_DETAILS.equals(type)){

			UserDetailsDto userDetailsVo = new UserDetailsDto();
			userDetailsVo.setLoginId(rs.getString("LOGIN_ID"));
			userDetailsVo.setUserId(rs.getInt("USER_ID"));
			return userDetailsVo;
		}
		else if(USER_ID_NAME.equals(type)){

			UserDetailsDto userDetailsVo = new UserDetailsDto();
			userDetailsVo.setLoginId(rs.getString("LOGIN_ID"));
			userDetailsVo.setUserId(rs.getInt("USER_ID"));

			return userDetailsVo;
		}*/

		//return null;
	}

}
