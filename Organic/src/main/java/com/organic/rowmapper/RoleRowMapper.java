package com.organic.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.organic.dto.RoleDetailsDto;


public class RoleRowMapper implements RowMapper<RoleDetailsDto>{

	public RoleDetailsDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		RoleDetailsDto dto = new RoleDetailsDto();
		dto.setRoleId(rs.getInt("ROLE_ID"));
		dto.setRoleName(rs.getString("ROLE_NAME"));
		return dto;
	}

}