package com.organic.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

public class CustomPasswordEncoder implements PasswordEncoder {	
	
	public String encode(CharSequence rawPassword) {
		String encodedPassword = null;
		try {
			encodedPassword = mySQLPassword(rawPassword);
		} catch (Exception e) {
			encodedPassword = null;
		}
		return encodedPassword;
	}
	
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		try {
			return encodedPassword.equals(mySQLPassword(rawPassword));
		} catch (Exception e) {
			return false;
		}
	}

	public String mySQLPassword(CharSequence rawPassword) throws Exception {
		String plainText = rawPassword.toString();
		byte[] utf8 = plainText.getBytes("UTF-8");
		byte[] test = DigestUtils.sha1(DigestUtils.sha1(utf8));
		return "*" + convertToHex(test).toUpperCase();
	}

	private String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}
}
